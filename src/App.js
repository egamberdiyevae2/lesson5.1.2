// !!!! Task

import React from "react";
// import User from "./user";
import "./app.css";

export default class App extends React.Component {
  state = {
    show: true,
  };

  add = () => {
    this.setState((state) => (state.show = true));
  };
  remove = () => {
    this.setState((state) => (state.show = false));
  };

  render() {
    return (
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          gap: "100px",
          border: "1px solid black",
          padding: "20px 30px",
          width: "500px",
          borderRadius: "15px",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            gap: "100px",
          }}
        >
          <button onClick={this.add}>Show</button>
          <button onClick={this.remove}>Hide</button>
        </div>
        {this.state.show ? (
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "flex-start",
              border: "2px solid green",
              width: "300px",
              height: "300px",
              borderRadius: "15px",
            }}
          >
            <h1
              style={{
                color: "greenyellow",
              }}
            >
              My Component
            </h1>
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }
}
